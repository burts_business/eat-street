<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Graze &amp; Feast | Street Food Market</title>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/fonts.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />
   <!--Novecento-Wide-->
   	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/stylesheet.css" type="text/css" charset="utf-8" />
   	
   	<link rel="icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />
   	<link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />

   	<link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>


    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	 <div class="inner-wrap">  

		<header class="header-home desktop">		
			
			<nav class="content container" id="top-menu">
				<ul class="left-menu">
					<h1><li><img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Eat Street" title="Eat Street" /></li></h1>
				</ul>
				<ul class="social-menu">
					<li><a href="https://www.facebook.com/Eatstreetcollective.co.nz?fref=ts" target="_blank"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="Facebook" title="Facebook"/></a></li>
					<li><a href="https://instagram.com/grazeandfeast/" target="_blank"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/instagram.svg" alt="Instagram" title="Instagram"/></a></li>
					<li><a href="http://twitter.com/" target="_blank"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.svg" alt="Twitter" title="Twitter"/></a></li>
				</ul>
				<ul class="right-menu">
					<li class="active"><a href="#home">Home</a></li>
					<li><a href="#about">About Us</a></li>
					<li><a href="#vendors">Vendors</a></li>
					<li><a href="#events">Events</a></li>
					<li><a href="#gallery">Gallery</a></li>
					<li><a href="#location">Location</a></li>
				</ul>
				
			</nav> 
		</header>
		<header class="header-home mobile">		
			<nav class="content container" id="top-menu">
				<ul class="left-menu">
					<h1><li><img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Eat Street" title="Eat Street" /></li></h1>
				</ul>
				<ul class="social-menu">
					<li class="open">Menu</li>
						<li><a href="http://facebook.com/"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="Facebook" title="Facebook"/></a></li>
					<li><a href="http://instagram.com/"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/instagram.svg" alt="Instagram" title="Instagram"/></a></li>
					<li><a href="http://twitter.com/"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.svg" alt="Twitter" title="Twitter"/></a></li>
				</ul>
				<ul class="right-menu">
					<li class="active"><a href="#home">Home</a></li>
					<li><a href="#about">About Us</a></li>
					<li><a href="#vendors">Vendors</a></li>
					<li><a href="#events">Events</a></li>
					<li><a href="#gallery">Gallery</a></li>
					<li><a href="#location">Location</a></li>
				</ul>
								
			</nav> 
		</header>
		<img class="header-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-header.png" alt="Background Texture" />	
		<img class="banner" src="<?php bloginfo('stylesheet_directory'); ?>/images/banner.svg" alt="Launch July 3rd 2015" />	
		
		
					
		<section id="main-content" class="clear" role="document">