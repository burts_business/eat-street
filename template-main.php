<?php /* Template Name: Main */
get_header(); ?>


<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>  
		              
		<h1 class="title"><span><?php the_title(); ?></span></h1>
		<div class="body-copy">
			<?php the_content(); ?>
		</div>
	
	<?php endwhile; ?>
<?php endif; ?>

		<?php if ( is_page( 'Contact' ) ){ 
			 include 'contact.php';
		} ?>
		
		<?php if ( is_page( 'Merch' ) ){ 
			 include 'merch.php';
		} ?>
		
		<?php if ( is_page( 'Photos' ) ){ 
			 include 'photos.php';
		} ?>
		
		<?php if ( is_page( 'Music' ) ){ 
			 include 'music.php';
		} ?>
		
		
		

				
<?php get_footer(); ?>