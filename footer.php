		<div id="location" class="section copy">
			<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />	
			<div class="content container clear">
				<h2>Location</h2>
				<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line.svg" alt="line" />-->
				<div class="half">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3192.9782561957486!2d174.7669013!3d-36.8429968!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d47f93e413aab%3A0xbcba9a85547b2898!2s99+Quay+St%2C+Auckland%2C+1010!5e0!3m2!1sen!2snz!4v1434405249418" width="100%" height="350" frameborder="0" style="border:0"></iframe>
				</div>
			
				<div class="half">
					<h3>Graze &amp; Feast</h3>
					<?php the_field('location_dates'); ?>
					
					<h3>Drop us a line</h3>
					<a class="location-link" href="mailto:grazeandfeast@gmail.com">grazeandfeast@gmail.com</a>
					
					<h3>Opening Hours</h3>
					<p><?php the_field('opening_hours'); ?></p>
				</div>
				
				<div class="clear"></div>
				<footer id="footer">

				<div class="footer-content">
					<nav>
						<ul>
							<li><a href="#home">Home</a></li>
							<li><a href="#about">About Us</a></li>
							<li><a href="#vendors">Vendors</a></li>
							<li><img class="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Eat Street" title="Eat Street" /></li>
							<li><a href="#events">Events</a></li>
							<li><a href="#gallery">Gallery</a></li>
							<li><a href="#location">Location</a></li>
						</ul>
					</nav> 
		
					<div id="sponsors">
						<p>Special thanks to our sponsors</p>
						<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/garage.png" alt="Garage Project" title="Garage Project" />
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/yealands.png" alt="Yealands" title="Yealands" />
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/zeffer.jpg" alt="Zeffer" title="Zeffer" />
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/fairfax.png" alt="Fairfax" title="Fairfax" />-->
						<img src="<?php the_sub_field('sponsors_image'); ?>" alt="Sponsors" title="Sponsors" />
					</div>
					<div>
						<p class="copyright">&#169; Copyright Graze &amp; Feast <?php echo date("Y") ?></p>
					</div>
				</div>
			</footer>

			</div>	
		</div>
		</section>
	
					
	
	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
  
  
  
  <script src="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/instafeed.min.js"></script>

	
	<script type="text/javascript">
	    var userFeed = new Instafeed({
	        get: 'user',
	        userId: 1836610263,
	        accessToken: '16498346.467ede5.d26ab444ea9741ffbe8ddff4c549ef91',
	        resolution: 'standard_resolution',
			limit: 8
	    });
	    userFeed.run();
	</script>


	
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/brooke.js"></script>
	
	 <!-- FlexSlider -->
	  <script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider.js"></script>
	
	  <script type="text/javascript">
	    $(function(){
	      SyntaxHighlighter.all();
	    });
	    $(window).load(function(){
	      $('.flexslider').flexslider({
	        animation: "slide",
	        slideshow: true, 
	        slideshowSpeed: 4000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
	        start: function(slider){
	          $('body').removeClass('loading');
	        }
	      });
	    });
	  </script>
	 
	  
	  <!-- Syntax Highlighter -->
	  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/shCore.js"></script>
	  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/shBrushXml.js"></script>
	  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/shBrushJScript.js"></script>
	
	  <!-- Optional FlexSlider Additions -->
	  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/jquery.easing.js"></script>
	  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/jquery.mousewheel.js"></script>
	  <script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/demo.js"></script>
	
    
       
    
  
<?php wp_footer(); ?>
</body>
</html>