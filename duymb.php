<?php get_header(); ?>	
<div id="video-home">
	<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 120%;
	margin-left: -5%; width:110%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
	<div class='embed-container'><iframe frameborder="0" scrolling="no" seamless="seamless" webkitallowfullscreen="webkitAllowFullScreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen" id="okplayer" src="http://player.vimeo.com/video/109451779?api=1&amp;js_api=1&amp;title=0&amp;byline=0&amp;portrait=0&amp;playbar=0&amp;player_id=okplayer&amp;loop=1&amp;autoplay=1"></iframe>
	</div>
	<div class="home-content">
		<img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/home-logo.svg" alt="Eat Street Collective" />
		<h2>Auckland's new home <br>for good eating</h2>
	</div>
</div>

<div id="about" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/food-truck.svg" alt="Food Truck" />-->
		<h2>About Eat Street</h2>
		<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line.svg" alt="line" />-->
		<div class="half">
			<p>Eat Street is an independently owned and operated food market based at Queens Wharf on Aucklands stunning Waitemata Harbour. Home to Auckland’s widest selection of Food Trucks, stalls and culinary treats.</p>

			<p>We are committed to raising the bar for a street dining experience, reflective to the enthusiasm and grace of New Zealands farm & market culture. Our market brunch, deliveries, and catered events, echo the passion for cuisine, warm hospitality, and refined design. We honor food and community with innovation, style, and dignity.</p>
			
			<p>We hope to see you soon down at Eat Street.<p>
			
			
			<p><b>- The Eat Street Team</b></p>
		</div>
		<!--<img class="vert" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line-vert.svg" alt="line-vert" />-->
		<div class="half">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/about.jpg" alt="About Eat Street" />
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-2.jpg')">
	
</div>
<div id="vendors" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>Vendor Spotlight:</h2>
	
    
 		<ul class="slider">
			<li>
				<div class="flexslider">
				    <ul class="slides">
					    <li>
							<h3><?php the_sub_field('vendor_name'); ?></h3>
							<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line.svg" alt="line" />-->
							<div class="half">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/lucky-logo.png" alt="Lucky Taco Logo" />
							</div>
							<!--<img class="vert" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line-vert.svg" alt="line-vert" />-->
							<div class="half">
								<p>Nothing</p>
								<a href="#" class="button">Find out More</a>
							</div>
					    </li>
					    <li>
							<h3>The Lucky Taco</h3>
							<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line.svg" alt="line" />-->
							<div class="half">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/lucky-logo.png" alt="Lucky Taco Logo" />
							</div>
							<!--<img class="vert" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line-vert.svg" alt="line-vert" />-->
							<div class="half">
								<p>Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo nulla pariatuconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
								
								<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id estderfs laborum. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</p>
								<a href="#" class="button">Find out More</a>
							</div>
					    </li>
					    <li>
							<h3>The Ducky Taco</h3>
							<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line.svg" alt="line" />-->
							<div class="half">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/lucky-logo.png" alt="Lucky Taco Logo" />
							</div>
							<!--<img class="vert" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line-vert.svg" alt="line-vert" />-->
							<div class="half">
								<p>Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo nulla pariatuconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
								
								<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id estderfs laborum. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</p>
								<a href="#" class="button">Find out More</a>
							</div>
					    </li>
				    </ul>
				</div>
			</li>
		
		<?php endforeach; ?>
	    </ul>
		<?php endif; ?>
		
		<div class="clear"></div>
		<div class="link">
			<p>Wanna become a vendor at Eat Street? <a href="#">Click here</a> to get in touch!</p>
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-3.jpg')">

</div>
<div id="events" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>Upcoming Events<br> at Eat Street</h2>
		<!--<img class="border" src="<?php bloginfo('stylesheet_directory'); ?>/images/border-line.svg" alt="line" />-->
		<div class="half">
			<div class="event">
				<h4>Bingo Night</h4>
				<p>Wednesday 24th August</p>
				<p>7pm - 10pm</p>
				<p>$10 entry fee</p>
			</div>
			<div class="event">
				<h4>George FM Sessions</h4>
				<p>Featuring..</p>
				<p>Connor Nestor & DJ Harry</p>
				<p>Friday 26th August</p>
				<p>4pm - 12am</p>
				<p>$10 entry fee</p>
			</div>
		</div>
		<div class="half">
			<div class="event">
				<h4>Korean BBQ Day</h4>
				<p>Friday 20th September</p>
				<p>4pm - 12am</p>
				<p>$10 entry fee</p>
			</div>
			<div class="event">
				<h4>Sunday Supper Club</h4>
				<p>Sunday 31st September</p>
				<p>6pm - 9pm</p>
				<p>$10 entry fee </p>
				<p>(complimentary cocktailon arrival)</p>
			</div>
		</div>
		<div class="clear"></div>
		<div class="link">
			<p>Wanna host an event at Eat Street? <a href="#">Click here</a> to get in touch!</p>
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-4.jpg')">
</div>
<div id="gallery" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>Gallery</h2>

		<div id="instafeed"></div>

	    <div class="clear"></div>
		<div class="link">
			<p><a href="http://instagram.com/eatstreetnz">Follow us on Instagram</a></p>
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-5.jpg')">

</div>

<?php get_footer(); ?>