<?php get_header(); ?>

<div id="single-posts">
	<div class="clear">
		     <?php if (have_posts()) : ?>
		               <?php while (have_posts()) : the_post(); ?>  
		               
		               	<div class="post-container">  
			               
			               <div class="post-left half">	
				               <div class="image-placeholder"></div>
				               <div class="details">
					               <svg version="1.1" id="cross" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 38.974 38.667" enable-background="new 0 0 38.974 38.667" xml:space="preserve">
									<g>
										<circle fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" cx="19.757" cy="19.282" r="17.526"/>
										<line class="vert" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="19.757" y1="9.971" x2="19.757" y2="29.689"/>
										<line class="horiz" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="9.898" y1="19.83" x2="29.616" y2="19.83"/>
									</g>
									</svg>
						
									<div class="open">Expand for more details</div>
									    <div class="display clear">
										    <div class="third">
											<h3>Project:</h3>
										    <p>Karen Walker Lookbook</p>
										</div>
										<div class="two-thirds">
											<h3>Details:</h3>
											<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Sed posuere consectetur est at lobortis. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										</div>
									</div>
								</div>
									
								<div class="clear">
									<div class="third">
									   <div class="image-placeholder dark short"></div>
									</div>
									<div class="third">
									   <div class="image-placeholder short"></div>
									</div>
									<div class="third">
									   <div class="image-placeholder dark short"></div>
									</div>
								</div>
				               
				               <div class="project-nav half">
				               	 <a href="#">Previous Project</a>
				               </div>
				               <div class="project-nav half right text-right">
				               	<a href="#">Next Project</a>
				               </div>

			               </div>
			               <div class="post-right half">
				               <h1><?php the_title();?></h1>
				               <div class="caption"><p><?php the_field('caption'); ?></p></div>
				               <?php the_content();?>
					            <a class="button" href="#">Contact</a>
				              <!-- <div class="post-share">		
					               <p>Share this post</p>	 
					               
					               <a href="https://twitter.com/share?url=&text=<?php the_title(); ?>: <?php echo urlencode(get_permalink($post->ID)); ?> &via=username&count=horizontal" class="twitter"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter-rev.svg" alt="twitter" /></a>           		
					               		<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" target="blank" class="facebook"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-rev.svg" alt="facebook" /></a>
							 </div>
							 <div class="clear"></div>
							 <a class="button" href="<?php echo home_url(); ?>">Back to Home</a>
				               	 
			               </div>-->
			               
		               	</div>
		               <?php endwhile; ?>
		     <?php endif; ?>
	</div>
</div>	
		
<?php get_footer(); ?>