<?php get_header(); ?>	
<!--<div id="home" class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-1.jpg')">
	<div class="home-content">
		<img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/home-logo.svg" alt="Eat Street Collective" />
		<h2>Auckland's new home <br>for good eating</h2>
	</div>
</div>-->
<div id="video-home">
	<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 120%;
	margin-left: -5%; width:110%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
	<div class='embed-container'><iframe frameborder="0" scrolling="no" seamless="seamless" webkitallowfullscreen="webkitAllowFullScreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen" id="okplayer" src="http://player.vimeo.com/video/<?php the_field('homepage_video'); ?>?api=1&amp;js_api=1&amp;title=0&amp;byline=0&amp;portrait=0&amp;playbar=0&amp;player_id=okplayer&amp;loop=1&amp;autoplay=1"></iframe>
	</div>
	<div class="home-content">
		<img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/home-logo.svg" alt="Eat Street Collective" />
		<h2>Auckland's new home <br>for good eating</h2>
	</div>
</div>

<div id="about" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>About Graze &amp; Feast</h2>
		<div class="half">
			<?php the_field('about_us'); ?>
		</div>
		<div class="half">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/about.jpg" alt="About Eat Street" />
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-2.jpg')">
	
</div>
<div id="vendors" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>Vendor Spotlight:</h2>
		
		<ul class="slider">
			<li>
				<div class="flexslider">
				    <ul class="slides">
					    <?php if( have_rows('vendors') ):
							while ( have_rows('vendors') ) : the_row();?>
						    <li>
								<h3><?php the_sub_field('vendor_name'); ?></h3>
								
								<div class="half">
									<img src="<?php the_sub_field('vendor_logo'); ?>" alt="Lucky Taco Logo" />
								</div>
								<div class="half">
									<?php the_sub_field('vendor_about'); ?>
									<a href="<?php the_sub_field('vendor_link'); ?>" class="button" target="_blank" >Find out More</a>
								</div>
						    </li>
						    <?php endwhile;
							    else :
							    endif; ?>    
				    </ul>
				</div>
			</li>
		</ul>
		
		<div class="clear"></div>
		<div class="link">
			<p>Wanna become a vendor at Graze &amp; Feast? <a href="#">Click here</a> to get in touch!</p>
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-3.jpg')">

</div>
<div id="events" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>Upcoming Events<br> at Graze &amp; Feast</h2>
		
		<?php if( have_rows('events') ):
			while ( have_rows('events') ) : the_row();?>
			<div class="event">
				<h4><?php the_sub_field('event_name'); ?></h4>
				<p><?php the_sub_field('event_date'); ?></p>
				<p><?php the_sub_field('event_time'); ?></p>
				<p><?php the_sub_field('event_cost'); ?></p>
			</div>
		 <?php endwhile;
	    else :
	    endif; ?>  
			
		<div class="clear"></div>
		<div class="link">
			<p>Wanna host an event at Graze &amp; Feast? <a href="#">Click here</a> to get in touch!</p>
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-4.jpg')">
</div>
<div id="gallery" class="section copy">
	<img class="paper-top" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-top.png" alt="Background Texture" />
	<div class="content container clear">
		<h2>Gallery</h2>

		<div id="instafeed"></div>

	    <div class="clear"></div>
		<div class="link">
			<p><a href="http://instagram.com/eatstreetnz">Follow us on Instagram</a></p>
		</div>
	</div>	
	<img class="paper-bottom" src="<?php bloginfo('stylesheet_directory'); ?>/images/bkg-about-bottom.png" alt="Background Texture" />
</div>
<div class="section image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/bkg-5.jpg')">

</div>

<?php get_footer(); ?>